const { SerialPort } = require('serialport');
const port = new SerialPort({ path: '/dev/ttyUSB0', baudRate: 115200, databits: 8 })

var buffer = new Buffer.alloc(4);

const SetPavementLight = (data) => {
    if (data.message == 0) {
        buffer[0] = 0x00; {/* Red */}
        buffer[1] = 0xff; {/* Green */}
        buffer[2] = 0x00; {/* Blue */}
    } else if (data.message == 1){
        buffer[0] = 0xff; {/* Red */}
        buffer[1] = 0x00; {/* Green */}
        buffer[2] = 0x00; {/* Blue */}
    } else {
        buffer[0] = 0x00; {/* Red */}
        buffer[1] = 0x00; {/* Green */}
        buffer[2] = 0x00; {/* Blue */}
    }
    buffer[3] = buffer[0] + buffer[1] + buffer [2]; {/* Kontrollsumma */}

    port.write(buffer, function(err) {
    if (err) {
        return console.log('Error on write: ', err.message)
    }
    })
};

io.on("connection", (socket) => {
    socket.on("detection_message", (data) => {
        SetPavementLight(data);
    });
});